#!/bin/sh
echo ""
echo "Welcome to NadekoBot."
echo "Downloading the latest installer..."
root=$(pwd)

if ! [ -x "$(command -v wget)" ]; then
    echo -e "wget is not installed please make sure it is installed before running!"
fi

rm "$root/n-menu.sh" 1>/dev/null 2>&1
wget -N https://gitlab.com/lumarel/nadeko-bash-installer/-/raw/v4/n-menu.sh

bash n-menu.sh
cd "$root"
rm "$root/n-menu.sh"
exit 0
